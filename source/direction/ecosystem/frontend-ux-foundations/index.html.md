---
layout: markdown_page
title: "Category Direction - Frontend & UX Foundations"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Create](/direction/dev) | 
| **Maturity** | [Viable](/handbook/product/categories/maturity/) |

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AFE%2FUX%20Foundations)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AFE%2FUX%20Foundations)
- [Overall Direction](/direction/ecosystem/)

_This direction is a work in progress, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and 
  epics on this category page. Sharing your feedback directly on GitLab.com is 
  the best way to contribute to our vision.
* Please share feedback directly via [email](mailto:pdeuley@gitlab.com), 
  [Twitter](https://twitter.com/gitlab) or on a video call. If you're a GitLab 
  UI or Pajamas user and have direct knowledge of your needs, we'd especially 
  love to hear from you.

## Overview

The Frontend & UX Foundations team is responsible for leading the direction of the experience design, visual style, and technical tooling of the GitLab product. This category encompasses two broad areas of focus: 

1. Tooling-focused enhancements
1. User-focused enhancements

Tooling-focused enhancements include:

* Improving webpack and optimizing frontend assets
* Improving the debugging experience, linters, and documentation with an emphasis on performance
* Shepherding migrations away from deprecated technologies
* Reducing technical debt
* Addressing [Progressive Web Application](https://developers.google.com/web/progressive-web-apps) needs

User-focused enhancements include:

* Building a cohesive and consistent user experience, visually and functionally
* Providing comprehensive usage guidelines, reusable components, content standards, and usability documentation
* Building in [accessibility](https://www.w3.org/WAI/)
* Reducing user experience and design debt

Both of these areas lead to a greater user and contributor experience while also increasing operational efficiencies. Our goal with the foundations team is to treat the underlying foundation of GitLab as a first-class internal product which supports product designers, engineers, and product managers to more efficiently perform their roles.

## Direction

As the GitLab product expands to include offerings for the entire DevOps lifecycle, it is critical to provide support for building a cohesive product that has the ability to replace disparate DevOps toolchains.

To serve these needs, we will work with the [Groups and Categories](/handbook/product/categories/) 
across GitLab to contribute to our design system, [Pajamas](https://design.gitlab.com/), while also continuing to define guidelines and best practices that will inform how these teams are designing and building products. Additionally, this team will act as a centralized resource, helping to triage large scale technical and experience problems as the need arises.

## Target Audience

Foundations is focused on supporting internal users and product teams, with a focus on four cross-functional counterparts: Product Designers, Technical Writers, Engineers, and Product Managers.

<!--
Todo: Add details about each audience
-->

We also aim to improve the community contributor experience by streamlining the process of writing consistent code that conforms to set practices.

<!--
### Where we are Headed
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

## What's Next & Why

**In progress:** [Creating, building, and styling foundation components](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/boards/1261225?&label_name[]=OKR). We identified 36 foundational components that are central to building and maintaining features at GitLab. In order to streamline the process of implementing components, we've defined four stages of a component lifecycle: Create, Build, Style, and Integrate. This first effort is aimed at completing the first three stages (create, build, and style) of the 36 foundational components. This will allow designers and engineers to have a robust system to draw from when designing and building GitLab products.

**In progress:** [Moving our Pajamas UI Kit from Sketch to Figma](https://gitlab.com/gitlab-org/gitlab-design/-/boards/1511008?&label_name[]=Figma). The move to Figma allows for greater collaboration, as well as community contributions. Currently, Sketch is only available on Mac platforms and there is no real-time collaboration features. Figma will allow us to provide a UI Kit that is available across platforms, while being available for community contributors to utilize for free. It will also promote collaboration through its use of real-time editing capabilities and version history. We will also be able to streamline developer handoff by simply linking to the design file, reducing the need for additional plugins such as Sketch Measure.

**Next:** Creating a comprehensive action plan for integrating components into the GitLab product.

**Next:** Auditing and updating our existing VPAT.

**Next:** Building comprehensive accessibility standards into our workflows.

**Later:** Building a dark theme.

### What we're not doing

**Building and integrating all components across GitLab.** The scope of this category is to provide guidance and governance for our design system and related tooling, and is staffed with dedicated UX designers to support that. However, _creating_ those components and implementing them throughtout the application is a massive lift that requires participation from every [Group and Category](/handbook/product/categories/). While FE/UX Foundations has _some_ Frontend Engineering capacity, it can't be responsible for creating and implementing everything.

## Maturity Plan
Today, we consider our **FE/UX Foundations** to be [Viable](/handbook/product/categories/maturity/). Below is how we think we'll grow that maturity level over time:

* **Viable:** A centralized system exists for product teams to contribute cohesive and consistent assets that aid in building the GitLab product. Documentation is in place to help offer guidance. Some reusable components exist and adhere to usage guidelines. Accessibility standards are followed in some cases. _(Where we are today)_
* **Complete:** A centralized system exists for product teams to contribute cohesive and consistent assets that aid in building the GitLab product. Documentation is in place to help offer guidance and these docs are consistently disseminated, enabling product teams to make autonomous decisions about component usage. Almost all reusable components exist, adhere to usage guidelines, and are referenced as the single source of truth. Some components are fully integrated into the GitLab product. Accessibility standards are followed in most cases.
* **Lovable:** A centralized system exists for product teams to contribute cohesive and consistent assets that aid in building the GitLab product. Documentation is in place to help offer guidance and these docs are consistently disseminated, enabling product teams to make autonomous decisions about component usage. Almost all reusable components exist, adhere to usage guidelines, and are referenced as the single source of truth. Almost all components are fully integrated into the GitLab product. Accessibility standards are followed in almost all cases.

<!--
### User success metrics
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

<!--
### Why is this important?
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

<!--
### Competitive Landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-management/process/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!--
### Analyst Landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!--
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!--
### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!--
### Top internal customer issue(s)
These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.-->

<!--
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
