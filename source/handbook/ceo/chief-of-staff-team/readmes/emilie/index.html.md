
layout: handbook-page-toc
title: "Emilie Schario's README"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

**Emilie Schario, Internal Strategy Consultant, Data** 

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Guiding principles
* I will create opportunity for myself and others. 
* Life is easier when we play like we're on the same team. 
* If you walk by a straw wrapper on the floor, pick it up. 

You don't have to solve all the problems of the universe. 
Just the one sitting right in front of you. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/T0NawB8j2q0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Bookmarks Bar

Optimizing the bookmarks bar in my Browser has really made my life much easier. I have never used the Bookmarks Bar before (and still don't on my personal laptop). From left to write, here's what lives on my Bookmarks Bar:

* The Efficiency Improvements Doc 
* Company Agenda
* E-Group Meeting Agenda
* Group Conversation Agenda
* Create New Issue in Data Project
* Data page in the handbook
* Change video speed
* MRs assigned to me
* MRs authored by me but not assigned to me, so I can follow up with the assignees
* Sid's MRs
* My 1:1 doc
* Create New Issue in the Handbook Project
* KPI page in the handbook
* CoS Team page in the handbook
* (Changes based on what I'm working on)

## Technical Workflow

I wrote [the data team's onboarding script](https://gitlab.com/gitlab-data/analytics/blob/master/admin/onboarding_script.sh) based mostly on my own workflow preferences. 

* I use iTerm2 as a terminal client with tmux 
* I use atom as my text editor, but I'm working to transition to vim
* Since I don't use my work email for email, I use it as my GitLab notification manager. I don't use GL todos.

## Personality

I love taking personality tests.
Even though they're descriptive not prescriptive, they make it easier to communicate with other folks about how I generally approach situations or interactions.
Personality frameworks do not give people excuses to be jerks.

#### DISC Assessment
Here is my DISC Assessment from July 2019:
[DISC Profile for July 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/disc_profile_2019_july.pdf)

Here is my DISC Assessment from June 2015 (to see the very little change):
[DISC Profile for June 2015](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/disc_profile_2015_june.pdf) 

#### Strengths Finder
Here is my Strengths Finder Report from December 2016:
[Strengths Finder Report from December 2016](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup_Strengths_Finder.pdf) 
Additional details can be found in my [Insight Report](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup Insight Report.pdf) and [Insight and Action Plan](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup Action Plan.pdf).

#### Gretchen Rubin's Four Tendencies
I am an Obliger. Gretchen Rubin outlines [The Four Tendencies](https://gretchenrubin.com/2013/10/what-kind-of-person-are-you-the-four-rubin-tendencies/).
She also speaks about [Obligers](https://youtu.be/CUU99WhRu5Q) on YouTube.

#### Enneagram
I am a Type 3, Wing 4.
This type is often called The Performer.
Read more about [Type 3](https://www.enneagraminstitute.com/type-3/) from the Enneagram Institute.

#### Myers-Briggs
I am an ENTJ. 
This type is often called The Commander.
Read more about [ENTJs](https://www.16personalities.com/entj-personality).

## I'm a competitive person
This isn't a weakness or a strength, by default, as it can be both.
I like lifting because it's the simplest way to see that measurable input leads to a certain measurable output. 

## Personal Weaknesses
While the list of *areas to improve* is long- infinite, even- I thought I'd take a second to highlight three weaknesses that have the most negative impact on my work.
I've tried to internalize the GitLab Iteration value, especially this line from [Focusing on Improvement](https://about.gitlab.com/handbook/values/#focus-on-improvement), "We believe great companies sound negative because they focus on what they can improve, not on what is working."
I believe the same can be true at the individual level.

Here are some feedback pieces I've received while at GitLab:
   * [Feedback Doc from January 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/2019 Recommendations for Emilie - Google Docs.pdf): This came from my manager when I had been at GitLab for about 6 months.
   * [360 Feedback from January 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/culture_amp_360_jan_2019.pdf): This came through CultureAmp when GitLab had an organization-wide 360.
   * [Feedback Doc from May 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Emilie Schario Handoff Doc - Google Docs.pdf): This came in the context of a manager handoff. I had been at GitLab for 11 months.
   * [My own reflection on 1 Year at GitLab in June 2019](http://shedoesdatathings.com/post/1-year-at-gitlab/)

#### Active Listening
It is my tendency to want to offer a thought as soon as it pops into my head.
I am working to address this by:
   * counting the number of times in a day that I interrupt folks. I keep a tally in my notebook
   * setting a timer in meetings for a period of time in which I will not speak
   * staying on mute by default. This way unmuting becomes an intentional act
I still have a lot of room for improvement.

#### Estimations
I am terrible at estimating.
I have gotten better, but I always only imagine the best-case scenario.
Estimating is hard to begin with, add that I am bad *and* that I am optimistic, and you've got a sauce for terribleness.
I am working to address this by:
   * providing a best case and a worst case
   * avoiding estimating (being honest here!)
   * capturing how long it took me to actually complete things so that I have more examples to base estimations off

#### Saying Yes Too Often
I'm an [obliger](https://gitlab.com/emilie/emilie.gitlab.io/blob/master/README.md#gretchen-rubins-four-tendencies), so when people ask me for things, my answer is that I want to please.
Because I want to help everyone- I want to meet all external expectations set on me- I say yes without consideration of what I already have on my plate.
As a result, I end up working on much more than I should be working on, leading me to work on nights and weekends.

I am no stranger to hard work, in fact I pride myself on my ethic.
Saying yes, though, is a problem because I spend time working on the things that are not the things I should be spending my time on.
I have gotten better by combining my love for [GTD](https://gettingthingsdone.com/) with doubling down on the [Ivy Lee Method](https://jamesclear.com/ivy-lee).

That being said, I still have to learn that "No" is a complete sentence.

As an aside, I think that *at least part* of the reason I'm so bad at estimating is that I let things creep in that throw off my original estimation. 

## Related pages
