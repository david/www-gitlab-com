---
layout: handbook-page-toc
title: "Marketing Programs Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Program Managers

Marketing Program Managers focus on executing, measuring and scaling GitLab's marketing campaigns, landing a message strategically focusing on a target audience using channels such as email nurture, digital ads, paid and organic social, events, and more. Marketing programs works with Content Marketing and Product Marketing to activate content in the most effective manner to drive leads for SDRs and Sales. Webcasts, gated content, and Pathfactory strategy/best practices are owned by the Digital Marketing Programs team.

## Responsibilities

**Jackie Gragnola**  
*Manager, Marketing Programs*
* **Team Prioritization**: plan prioritization of campaigns, related content and webcasts, event support, and projects for the team
* **Hiring**: organize rolling hiring plan to scale with organization growth
* **Onboarding**: create smooth and effective onboarding experience for new team members to ramp quickly and take on responsibilities on the team
* **Transition of Responsibilities**: plan for and organize efficient handoff to new team members and between team members when prioritization changes occur

**Agnes Oetama**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Virtual Events**: project management, set up, promotion, and follow up of all virtual events (webcasts, demos, virtual sponsorship)
* **Bi-weekly Newsletter**: coordinate with cross-functional teams on topics and set up newsletter in marketo
* **APAC Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **APAC Corporate Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)

**Jenny Tiemann**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - Central & PubSec Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Acceleration and ABM Campaigns**: organize execution, timeline, and campaign tracking.
* **Nurture Campaigns**: strategize and set up campaigns (email nurturing)

**Zac Badgley**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - East Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Nout Boctor-Smith**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Corporate Worldwide Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)
* **Ad-Hoc Emails**: coordination of copy, review, and set up of one-time emails (i.e. security alert emails, package/pricing changes)

**Megan Mitchell**
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - West Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Eirini Panagiotopoulou**
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Corporate EMEA Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams
* **EMEA - Southern Europe & MEA Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Indre Kryzeviciene**
*Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **EMEA - UK/I, Northern Europe & Russia, Central Europe & CEE Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team

**Marketing Operations**
* Cleaning and uploading of lead lists post-event
* Creation of geo target lists in SFDC for email promotion of events

*Each team member contributes to making day-to-day processes more efficient and effective, and will work with marketing operations as well as other relevant teams (including field marketing, content marketing, and product marketing) prior to modification of processes.*

### Order of assignment for execution tasks:
{:.no_toc}
1. Primary responsible MPM  
1. Secondary MPMs  (if primary MPM is OOO or not available)
1. Marketing OPS (if MPMs are OOO or not available)

### Holiday coverage for S1 security vulnerabilities email communication

In the event of an S1 (critical) security vulnerability email communication is needed during the holidays, please create an issue using *[Email-Request-mpm template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/Email-Request-mpm.md)* and ping in #marketing-programs tagging @marketing-programs

## Email marketing calendar

The calendar below documents the emails to be sent via Marketo and Mailchimp for:
1. event support (invitations, reminders, and follow ups)
1. ad hoc emails (security, etc.)
1. webcast emails (invitations, reminders, and follow ups)
1. milestones for nurture campaigns (i.e. when started, changed, etc. linking to more details)

*Note: emails in the future may be pushed out if timelines are not met for email testing, receiving lists from event organizers late, etc. The calendar will be updated if the email is pushed out. Please reference the MPM issue boards (described below on this page) to see progress of specific events/webcasts/etc.*

<figure>
  <iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## Email nurture program

### Visualization of current nurture streams

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/7889a7fb-e1f0-4c67-92ae-7becd009625f" id="XA5ojeoO~Tej"></iframe></div>

### Top funnel nurture

The goal of the top funnel email nurture is to further engage early stage leads so they become MQLs and/or start a free trial. Currently the top funnel nurture is segmented into 3 different tracks : Cloud native, CI/CD and Reducing cycle times.

High level overview on the messaging for each track:

- Cloud Native: The messaging for this track is centered around benefits of cloud native transformation (Kubernetes, containers and the likes) and how GitLab can help. This track is suitable for those that consumed cloud native assets off our website/partner sites and those that attended cloud native themed offline/online events.
- CI/CD: The messaging for this track is centered around the benefits of GitLab's powerful built-in CI/CD, why this is better than a traditional plug-in CI/CD solution. This track is suitable for those that consumed CI/CD assets off our website/partner sites and those that attended offline/online CI/CD themed events.
- Reduce cycle time: The messaging for this track is centered around the benefits of reducing cycle time and how GitLab can help to accomplish that. This is the more generic catch-all track.

The top funnel emails are sent out on a weekly basis on Tuesdays at 7 a.m. local time.

*Note: A lead will be transferred to the stream related to the latest asset they downloaded. For example if lead A is already in the cloud native stream and they registered for the CI/CD webcast, they will move to the CI/CD stream and start receiving the first email in that stream.  Currently, this is the best way to align content to the prospect's most current topic of interest. The framework is subject change if we eventually categorize assets by high-intent vs. low intent or if we bring on a content insight and activation platform.*

**[>>  Email copies for TOFU nurture](https://docs.google.com/presentation/d/1EzIFyaJB5N_bZ6qmP89Y_h4IGRp048MPzQ5vqhmTNrM/edit#slide=id.g29a70c6c35_0_68)**

### Integrated campaign nurture

Please note integrated campaign nurture is NOT considered top of funnel. Please see the linked epics for campaign briefs for more detail on audience, messaging, and value driver points.

High level overview on the messaging for each track:

- [Increase Operational Efficencies](https://gitlab.com/groups/gitlab-com/marketing/-/epics/367): The messaging for this track is centered around simplifying the software development toolchain to reduce total cost of ownership. This track is targeted towards directors, managers, tools group, IT platforms in the functional area of development, DevOps, and IT Ops. 
- [Deliver Better Products Faster](https://gitlab.com/groups/gitlab-com/marketing/-/epics/363): The messaging for this track is centered around accelerating the software delivery process to meet business objectives. This track is targeted towards VP, director, lead, head, manager level in the functions of applications and development. 
- [Reduce Security and Compliance Risk](https://gitlab.com/groups/gitlab-com/marketing/-/epics/368): The messaging for this track is centered around simplifying processes to comply with internal processes, controls and industry regulations without compromosing speed. This track is targeted towards manager level in the functions of security, risk management, and application security. 
- [CI Build and Test Auto](https://gitlab.com/groups/gitlab-com/marketing/-/epics/379): The messaging for this track is centered around eliminiating your messy toolchain and getting fully integrated CI/CD out-of-the-box with GitLab. It is not targeted towards a specific CI competitor. This track is targeted towards director, tools owner, and chief/prinicipal architects in the functions of applications, development, QA, and DevOps.
- [Jenkins Take Out](https://gitlab.com/groups/gitlab-com/marketing/-/epics/282): The messaging for this track is centered around why GitLab built-in CI/CD solution is a better alternative than Jenkins plug-in solution. This track is targeted towards director, tools owner, and chief/prinicipal architects in the functions of applications, development, QA, and DevOps.


### SaaS trial nurture

SaaS Gold trial nurture communication are sent via Marketo and Outreach throughout the 30-day free trial period.

**Goal of the Marketo nurture:** Educate trialers on key features within GitLab Gold SaaS tier.

**Goal of SDR Outreach nurture:** Qualify and meetings setting for SaaS Gold trialers.

**[>> Email copies for SaaS Gold package trial nurture](https://gitlab.com/groups/gitlab-com/marketing/-/epics/98)**

### Self-hosted trial nurture

Self Hosted Ultimate trial nurture communication are sent via Marketo and Outreach throughout the 30-day free trial period.

**[>> Email copies for Self-hosted Ultimate package nurture](https://docs.google.com/presentation/d/1KSAZFwz3nvSTIXOP8urGWW6dJWhtpawVKFcaoFLDPdg/edit#slide=id.g2ae1ad1112_0_22)**

## Marketo vs. PathFactory

### Summary: Pathfactory ≠ Nurture. Pathfactory is a tool that - instead of driving to a single asset - drives to a related-content adventure.

Nurture is a channel to bring an individual to the content. Just like ads, social, website, etc. drive to CTAs, Pathfactory link is the CTA - a *powerful* CTA because it can lead the individual down a "choose your own adventure" path of consumption which we track.

### FAQ:

1. What is the Goal of PathFactory and Marketo?

>**Marketo nurture:** To keep the conversation going about GitLab (keep us top of mind).

>**PathFactory:** To increase consumption/activation of GitLab content.

1. Do the lead lifecycle stage play into each nurture (For example, do we only want leads with status "Inquiry" to receive PathFactory nurture and leads with status "MQL" to only receive Marketo nurture?) ?

>No, both Marketo and PathFactory can be leveraged to help push leads further down the funnel.

1. Can records be in Marketo nurture and PathFactory nurture at the same time? If not, is 1 prioritized over the other?

>Yes, the PathFactory track acts as a supplement to the existing Marketo nurture instead of a replacement.

1. How does the post event follow up process look like? Specifically, when will it be appropriate to add leads to an existing Marketo nurture vs. just linking to a PathFactory track in the follow up email?

>1) **For event leads where there is no relevant Marketo nurture:** In the follow up email, attach a link to **a piece of content** in a relevant PathFactory track.

>2) **For event leads where a relevant Marketo nurture exists**: In the follow up email, attach a link to a piece of content in a relevant PathFactory track that is **not referenced in the Marketo nurture**. Additionally, if/when requested by the FMM team, we should also add the leads to the Marketo nurture to keep the conversation going.

You can find more information about using PathFactory in the [Marketing Operations handbook](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/).

# How we work together

## Issue management in GitLab

### How our team stays productive
Below are issue views that the Marketing Program Team monitors for in-progress and upcoming action items:
* [MPM Priority](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20Priority)
* [Landing Pages](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=✓&state=opened&label_name[]=MPM%20-%20Landing%20Page%20%26%20Design)
* [Invitations & Reminder Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Invitations%20%26%20Reminder)
* [Follow Up Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Follow%20Up%20Emails)
* [Add to Nurture](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Add%20to%20Nurture)
* [MPM - Radar](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Radar)

### Marketing Programs labels in GitLab

* **Marketing Programs**: General labels to track all issues related to Marketing Programs. This brings the issue into the board for actioning.
* **MPM - Radar**: Holding place for any issues that will need Marketing Program Manager support, including gated content, events, webcasts, etc.
* **MPM - Supporting Epic / Issue Created**: Indicates that the epic and supporting issues were created for the MPM - Radar issue. At the time this label is applied, the "MPM - Radar" label will be removed.
* **MPM - Secure presenters and schedule dry runs**: Used when MPM is securing presenters and Q&A support for an upcoming virtual event.
* **MPM - Landing Page & Design**: Used by Marketing Program Manager to indicate that the initiative is in the stage of landing page creation and requesting design assets from the web/design team.
* **MPM - Marketo Flows**: Used by Marketing Program Manager to indicate that the initiative is in the stage of editing/testing of flows in Marketo.
* **MPM - Create Target List**: Used by Marketing Program Manager requested of Marketing Ops and in collaboration with Field Marketing to receive a list curated for the geo target. Marketo smart lists for larger metro areas around the world are built to expedite list creation. Additional curation done in Salesforce.
* **MPM - Invitations & Reminder**: Used by Marketing Program Manager when the initiative is in the stage of identifying segmentation to target and outreach strategy.
* **MPM - Follow Up Emails**: Used by Marketing Program Manager when initiative is in the stage of writing and reviewing relevant emails (reminders, follow up, etc.).
* **MPM - Add to Nurture**: Used by Marketing Program Manager when initiative is in the stage of being added to nurture.
* **MPM - Project**: For non-campaign based optimizations, ideation, and projects of Marketing Program Managers
* **MPM - Blocked/Waiting**: Designates that the MPM is blocked by another team member from moving forward on the issue.
* **MPM - Switch to On-demand**: Used by Marketing Program Manager when switching webcast landing page and subsequent marketo programs to on-demand post event.
* **MPM Priority**: to be used by MPMs to organize their top priority tasks. These are not to be applied by other team members.

# Marketing Programs support requests

## Requesting to "Gate" a Piece of Content

This section has been updated and moved to the [gated-content page.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content)

### A visual of what happens when someone fills out a form on a landing page

![image](/images/handbook/marketing/marketing-programs/landing-pages-flow-model.png)

❌ **A landing page with a form should never be created without the inclusion and testing by Marketing Programs and/or Marketing Ops.**

Please post in the [slack channel #marketing-programs](https://gitlab.slack.com/messages/CCWUCP4MS) if you have any questions.

## Marketing Programs and Virtual (Online) Events support

This section has been updated and moved to the [virtual-events page.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/)

## Marketing Programs and Event Support

This section has been updated and moved to the [events page.](/handbook/marketing/events/)

Some quick links to specific sections you may be looking for:
* [Campaign tags for events](/handbook/marketing/events/#all-events---setting-up-the-campaign-tag)
* [Timelines and SLAs between Field Marketing and Marketing Programs](/handbook/marketing/events/#timelines-and-slas-between-field-marketing-and-marketing-programs)
* [Timelines and SLAs between Corporate Marketing and Marketing Programs](/handbook/marketing/events/#timelines-and-slas-between-corporate-marketing-and-marketing-programs)
* [MPM process for event epics and related issues](/handbook/marketing/events/#mpm-steps-to-set-up-event-epic)
* [The different event types (conferences, field events, owned events, and speaking sessions)](/handbook/marketing/events/#offline-events-channel-types)
* [Adding an entry to `/events/` page](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)
* [Using the Marketo Check-in App](/handbook/marketing/events/#marketo-check-in-app)

## Requesting an Email  

Process to request an email can be found in the [Business OPS](/handbook/business-ops/resources/#requesting-an-email) section of the handbook.   

Primary party responsible for various email types can be determined using the [table above](#responsibilities).

**MPM Process for an ad-hoc email** (security, support, etc)

We use both Marketo and Mailchimp to send ad-hoc emails. Marketo is the primary system for all marketing emails and the regularly scheduled security updates. Mailchimp should be used for emails to gitlab.com users as these users are not in our marketing systems (unless they have signed up for content). Examples of emails to be sent through Mailchimp: Critical security updates, support updates that impact a specific subset of users, suspicious account activity notifications.

 - Once an email request is received using the process above, the MPM determines which system to send the email from. This is usually Mailchimp unless it is a marketing email to our database.
 - Log into Mailchimp and select "Audience."
 - Select "View Audiences" from the "Manage Audience" drop down on the right side of the screen.
 - Select "Create Audience" and name the audience using ISODate_CampaignName. Complete the Default From email address and name (usually info@, but can be security@ or support@ depending on the email). For "Remind people how they signed up to your audience" select "Entire Database" then click Save.
 - For instructions on adding contacts, review this [documentation](https://mailchimp.com/help/import-contacts-mailchimp/). You can also copy and paste the contacts if the list is small enough. You will be asked to map the fields on your import to the database fields prior to upload.
 - Select "Campaigns." Find a prior campaign that used the same type of email you want to use (plain text or regular). Security emails, privacy policy updates, and terms of service updates use plain text, support emails can use regular.
 - Select replicate, then select the audience you created above. If you do not have the list yet, you can select an existing audience and change it later during the review process.
 - Name the campaign using ISODate_CampaignName. Lay out the email as normal. For information about using Merge Tags, review this [documentation](https://mailchimp.com/help/getting-started-with-merge-tags/).
 - Send a test email to yourself first to confirm email is correct and the links work properly. Then, make any changes and send a test to the designated approvers.
 - Once fully approved, review the audience (and update if necessary), sender, subject line, email and schedule to send.
 
## Email Marketing Best Practices

**Content**
*  Email copy should be shorter and more conversion-oriented 
*  Avoid walls of text when possible
*  Use extremely clear wording and remove words that don't provide value
*  Minimize CTAs (calls-to-action)
*  Take advantage of content hierarchy
*  Use humor when it makes sense 
*  Craft compelling subject lines and employ the preview text as a complement to the subject line
*  Focus on value-first content and CTAs. Ask yourself: "what's in it for the subscriber?"

**Design**
*  Consider resposive design
*  Code all text in HTML
*  Minimize CTAs
*  Images should add to the goal of your email and not take away from it
*  An email is not a landing page
*  Consider accessibility

**A/B Testing**
*  Each test group should include at least 1000 people
*  You need a bigger test group if you're testing for click-through rate versus testing for open rate
*  Have a goal and idea regarding what you want to improve and how your test is going to help with that
*  Test _one_ variable at a time
*  Due to our small sample sizes, we recommend a full 50/50 split versus a 10/10/80 or 20/20/60 split
*  Remember your subject line or "from name" (testing open rates) could have an impact on click-through rate and conversion rate
*  Let the MPM know at the beginning of the project if you're interested in running an A/B test and what your goals/hypothesis is
*  Keep track of the split test learnings so we can learn and innovate!

## Requesting to add leads to the top funnel nurture

To add leads to the top funnel nurture, please reach out to one of the marketing program managers through an issue. The marketing program manager will assess the request and can add the leads to the proper stream via a 'Request Campaign' marketo flow step.

- In order to prevent people currently in a stream from switching streams due to a manual add to nurture, the MPM will exclude anyone in the '*Member of Engagement Stream*' Smart List. 'Member of Smart List' -> Person NOT IN '*Member of Engagement Stream*'. The '*Member of Engagement Stream*' Smart List includes anyone who has not exhausted content in their current stream or is not paused in a stream.
- Flow step to add leads to TOFU nurture 'Cloud Native' track -> 'request Smart Campaign: TOFU nurture.Add to stream 1: Cloud Native (trigger)'
- Flow step to add leads to TOFU nurture 'CI/CD' track -> 'request Smart Campaign: TOFU nurture.Add to stream 4: CI/CD (trigger)'
- Flow step to add leads to TOFU nurture 'Reduce Cycle Time (Generic DevOps)' track -> 'request Smart Campaign: TOFU nurture.Add to stream 2: Reduce Cycle Time_Forrester (trigger)'

## Requesting to add a new asset to the top funnel nurture

If you'd like to add an asset to the top funnel nurture, please create an issue in the [digital marketing programs](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?nav_source=navbar) project, explaining why you'd like the asset added and which stream you'd like it added to. Please assign the issue to @aoetama and ping her in the comment.

*Note: If the asset does not fit into one of the active streams, we will keep track of it as a wishlist and reference as input when we decide to roll out new streams in the future.*

# Programs reporting

MPM uses SFDC reports and dashboards to track program performance real-time. Data from the below SFDC reports/dashboards along with anecdotal feedback gathered during program retros will be used as guidelines for developing and growing various marketing programs.

The SFDC report/dashboard is currently grouped by program types so MPMs can easily compare and identify top performing and under performing programs within the areas that they are responsible for.

#### [Webcast dashboard](https://gitlab.my.salesforce.com/01Z6100000079e6)

The webcast dashboard tracks all webcasts hosted on GitLab's internal webcast platform. It is organized into 3 columns. The left and middle columns tracks 2 different webcast series (Release Radar vs. CI/CD webcast series). The right column tracks various one-off webcasts since Jan'18.

#### [Live Demo dashboard](https://gitlab.my.salesforce.com/01Z6100000079f4)

The LIVE Demo dashboard is organized into 2 columns. The left column tracks the bi-weekly Enterprise Edition product demos (1 hour duration). The bi-weekly Enterprise Edition product demos ran between Q1'18 - Q2'18.
The right column tracks the weekly high level product demo + Q&A session (30 minutes duration). The weekly high level product demo + Q&A session was launched in Q4'18 and currently running through the end of Feb 2019.

#### [Virtual Sponsorship dashboard](https://gitlab.my.salesforce.com/01Z61000000UD44)

### Key Metrics tracked in ALL virtual events dashboards

*Note: Virtual Events include Webcast, LIVE demos and Virtual Sponsorship*

**Total Registration :** The number of people that registered for the virtual event regardless whether they attend or not.

**Total Attendance:** The number of people that attended the LIVE virtual event (exclude people who watched the on-demand version).

**Attendance Rate:** % of people that attended the LIVE virtual event out of the total registered (i.e: Total Attendance / Total Registration).

**Net New Names:** The number of net new names added to our marketing database driven by the virtual event. Because a net new person record may be inserted into our CRM (SFDC) as a lead or a contact object therefore, we need to add `Total net new leads` and `Total net new contacts` to get the overall total net new names.

**Influenced Pipe:** Total New and Add-on business pipeline IACV$ influenced by people who attended the LIVE virtual event. The webcast and live demo dashboards currently use SFDC out of the box `Campaigns with Influenced opportunities` report type because Bizible was implemented in June'18 and therefore the attribution report did not capture data prior to this. We plan to migrate webcast and live demo influenced pipe reports to Bizible attribution report in the next dashboard iteration so they align with overall marketing reporting.

# Programs logistical set up

### Webcast

Webcast program set up can be found in the [Business OPS](/handbook/business-ops/resources/#logistical-setup) section of the handbook.

### Newsletter

#### Process for bi-weekly newsletter

Open an issue in the [Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/) using the `Newsletter` template, including the newsletter send date in the issue title.

Assign to `@aoetama` and a member of the editorial team, based on the following schedule:

- 8/25 - `@vsilverthorne`
- 9/10 - `@skassabian`
- 9/25 - `@vsilverthorne`
- 10/10 - `@skassabian`
- 10/25 - `@vsilverthorne`
- 11/10 - `@skassabian`
- 11/25 - `@vsilverthorne`
- 12/10 - `@skassabian`
- 12/25 - `@vsilverthorne`

#### Creating the newsletter in Marketo

A day or two before the issue due date, create the newsletter draft. It's easiest to clone the last newsletter in Marketo:

1. Go to Marketing Activities > Master Setup > Outreach > Newsletter & Security Release
1. Select the newsletter program template `YYYYMMDD_Newsletter Template`, right click and select `Clone`.
1. Clone to `A Campaign Folder`.
1. In the `Name` field enter the name following the newsletter template naming format `YYYYMMDD_Newsletter Name`.
1. In the `Folder` field select `Newsletter & Security Release`. You do not need to enter a description.
1. When it is finished cloning, you will need to drag and drop the new newsletter item into the appropriate subfolder (`Bi-weekly Newsletters`, `Monthly Newsletters` or `Quarterly Newsletters`).
1. Click the + symbol to the left of your new newsletter item and select `Newsletter`.
1. In the menu bar that appears along the top of your screen, select `Edit draft`.

#### Editing the newsletter in Marketo

1. Make sure you update the subject line.
1. Add your newsletter items by editing the existing boxes (double click to go into them). It's best to select the `HTML` button on the menu bar and edit the HTML so you don't inadvertently lose formatting.
1. Don't forget to update the dates in the UTM parameters of your links (including the banner at the top and all default items such as the "We're hiring" button).

#### Sending newsletter test/samples from Marketo

1. When you're ready, select `Email actions` from the menu at the top, then `Send sample` to preview.
1. Enter your email in the `Person` field, then in `Send to` you can add any other emails you'd like to send a preview too. We recommend sending a sample to the newsletter requestor (or rebecca@ from the content team for marketing newsletters) for final approval.
1. When you are satisfied with the newsletter, select `Approve and close` from the `Email actions` menu.

#### Sending the newsletter

1. When the edit view has closed, click on the main newsletter item in the left-hand column.
1. In the `Schedule` box, enter the send date and select `Recipient time zone` if the option is available.
1. Make sure `Head start` is checked too.
1. In the `Approval` box, click on `Approve program`.
1. Return to the newsletter issue and leave a comment telling requestor (@rebecca from the content team for marketing newsletters)  to double check all has been set up correctly. Close the issue when this is confirmed.
