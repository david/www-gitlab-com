---
layout: job_family_page
title: "Engineering Management"
---


## Engineering Management Roles at GitLab

Managers in Engineering at GitLab see their team as their product. While they are technically credible and know the details of what engineers work on, their time is spent safeguarding their team's health, hiring a world-class team, and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Engineering Manager

Unless otherwise specified below, all Engineering Manager roles at GitLab share the following responsibilities and requirements:

#### Responsibilities

* Help your engineers grow their skills and experience
* Author project plans for epics
* Run agile project management processes
* Conduct code reviews, and make technical contributions to product architecture as well as getting involved in solving bugs and delivering small features
* Actively seek and hire globally-distributed talent
* Conduct managerial interviews for candidates, and train the team to do technical interviews
* Contribute to the sense of psychological safety on your team
* Generate and implement process improvements
* Hold regular [1:1's](/handbook/leadership/1-1/) with all members of their team
* Give regular and clear feedback around the [individual's performance](/handbook/leadership/1-1/suggested-agenda-format)
* Foster technical decision making on the team, but make final decisions when necessary
* Draft quarterly OKRs and [Engineering KPIs](/handbook/business-ops/data-team/metrics/#engineering-kpis)
* Improve product quality, security, and performance

#### Requirements

* Exquisite brokering skills: regularly achieve consensus amongst departments
* 5 years or more experience
* 2 years or more experience in a leadership role with current technical experience
* In-depth experience with Ruby on Rails, Go, and/or Git, in addition to any experience required by the position's [specialty](#specialties)
* Excellent written and verbal communication skills
* You share our [values](/handbook/values), and work in accordance with those values
* Ability to use GitLab

#### Nice-to-have Requirement

* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Golang experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools

#### Performance Indicators

Engineering Managers have the following job-family performance indicators.

* [Response to Community SLO](/handbook/engineering/development/performance-indicators/#response-to-community-slo)
* [Hiring Actual vs. Plan](/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan)
* [Team/Group Throughput](/handbook/engineering/development/performance-indicators/#throughput)
* [Team/Group MR Rate](/handbook/engineering/development/performance-indicators/#mr-rate)
* [Diversity](/handbook/engineering/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/performance-indicators/#team-member-retention)

#### Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Engineering, Backend
* Next, candidates will be invited to schedule a 45 minute second peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute third interview with another member of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with a member of the Product team
* Next, candidates will be invited to schedule a 45 minute fifth interview with our VP of Engineering
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Senior Engineering Manager

The Senior Engineering Manager role extends the [Engineering Manager](#engineering-manager) role.

#### Responsibilities

- Manage up to three engineering teams
- Conduct managerial interviews for candidates, and train engineering managers to do said interviews
- Generate and implement process improvements, especially cross-team processes
- Hold regular [1:1s](/handbook/leadership/1-1/) with team managers and skip-level 1:1s with all members of their team
- Management mentorship

#### Requirements

- Technical credibility: Past experience as a product engineer and leading teams thereof
- Management credibility: Past experience (3 to 5 years) as an engineering manager
- Ability to understand, communicate and improve the quality of multiple teams
- Demonstrate longevity at at least one recent job
- Ability to be successful managing at a remote-only company
- Humble, servant leader

#### Nice-to-have Requirements

- Be a user of GitLab, or familiar with our company
- Prior Developer Platform or Tool industry experience
- Prior product company experience
- Prior high-growth startup experience
- Experience working on systems at massive (i.e. consumer) scale
- Deep open source experience
- Experience working with global teams
- We value diversity and inclusion in leadership
- Be inquisitive: Ask great questions

#### Performance Indicators

Senior Engineering Managers have all the [Engineering Manager Performance Indicators](#performance-indicators) attributed at the section level.

### Director of Engineering

The Director of Engineering role extends the [Senior Engineering Manager](#senior-engineering-manager) role.

#### Requirements

* Excellent communication skills
* Expert hiring manager skills and experience
* A strong people management philosophy for managers and engineers
* Experience managing managers
* Agile project management skills
* Ability to understand, communicate and improve the quality of multiple teams
* Demonstrate longevity at at least one recent job
* Ability to be successful managing at a remote-only company
* Humble, servant leader

#### Nice-to-have Requirements

* Be a user of GitLab, or familiar with our company
* Prior Developer Platform or Tool industry experience
* Prior product company experience
* Prior high-growth startup experience
* Experience working on systems at massive (i.e. consumer) scale
* Deep open source experience
* Experience working with global teams
* We value diversity and inclusion in leadership
* Be inquisitive: Ask great questions

#### Performance Indicators

Director of Engineering have all the [Senior Engineering Manager Performance Indicators](#performance-indicators) attributed at the section level.

### Senior Director of Engineering

The Senior Director of Engineering role extends the [Director of Engineering](#director-of-engineering) role.  This role is defined by the functional area(s) the person manages.

* Organizational credibility: Past experience in managing an entire functional area of Engineering
* Prioritzation of hiring efforts to focus on areas of most need and quickly recruit top engineering talent
* Motivate and communicate across multiple levels of their department
* Have successful peer partnerships with other department leaders in Engineering, and cross-functionally (Product Management, sales, marketing, alliances, etc)
* Provide a consistent/successful interface between Engineering Development and Product Management
* Development, measurement, and management of key metrics for functional area's performance
* Drive high throughput
* Standardize the development process where needed, allow local differences where advantages
* Help shift the organization toward CD over time

#### Performance Indicators

Senior Director of Engineering have all the [Director of Engineering Performance Indicators](#performance-indicatorstors) attributed at the department level.

### Executive VP of Engineering

* Drive recruiting of a world class team
* Help their directors, senior managers, managers, and engineers grow their skills and experience
* Measure and improve the happiness of Engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance
