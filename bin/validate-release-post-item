#!/usr/bin/env ruby
#
# Validate release post items conform to the proper format
#
# Examples:
#   bin/validate-release-post-item
#   bin/validate-release-post-item [filename/dir]

require 'yaml'
load 'bin/Rx.rb'

STAGES_PATH = File.join('data', 'stages.yml')
CATEGORIES_PATH = File.join('data', 'categories.yml')

module ReleasePostHelpers
  Abort = Class.new(StandardError)
  Exception = Class.new(StandardError)
  Done = Class.new(StandardError)

  def fail_with(message)
    raise Exception, "\e[31merror\e[0m #{message}"
  end

  def abort_with(message)
    raise Abort, "\e[31merror\e[0m #{message}"
  end

  def titleize(x)
    "#{x.split.each{|word| word.capitalize!}.join(' ')}"
  end
end

class ReleasePostEntry
  include ReleasePostHelpers

  def initialize(file_path, schema)
    @file_path = file_path
    @schema = schema

    f = File.open(file_path).read
    begin
      @item = YAML.load(f)
    rescue Exception
      fail_with "#{@file_path}: invalid YAML"
    end
  end

  def execute
    assert_validates_schema


    if feature?
      assert_stage_key feature['stage']

      names = feature['categories']
      names.each { |n| assert_category_name n }

      assert_image
    end

    puts "\e[32mpass\e[0m #{@file_path}"
  end

  def feature?
    @item.has_key? 'features'
  end

  def feature
    (@item['features']['top'] || @item['features']['primary'] || @item['features']['secondary'])[0]
  end

  def assert_validates_schema
    begin
      @schema.check!(@item)
    rescue Exception => e
      fail_with "#{@file_path}: #{e.message}"
    end
  end

  def assert_stage_key(key)
    stages = YAML.load_file(STAGES_PATH)['stages'].map { |key, value| [key, value['display_name']] }.to_h
    stage_keys = stages.keys

    unless stage_keys.include? key
      message = "#{@file_path}: invalid stage, key '#{key}' not found in #{STAGES_PATH}"

      # Check for common mistakes
      suggested_key = key.downcase.tr(' ', '_') if stage_keys.include? key.downcase.tr(' ', '_')
      message << "\n  Did you mean '#{suggested_key}'?" if suggested_key

      fail_with message
    end
  end

  def assert_category_name(name)
    categories = YAML.load_file(CATEGORIES_PATH).map  { |key, value| [key, value['name']] }.to_h
    category_names = categories.values

    unless category_names.include? name
      message = "#{@file_path}: invalid category, name '#{name}' name not found in #{CATEGORIES_PATH}"

      # Check for common mistakes
      suggested_category =
        categories[name] ||
        categories[name.downcase.tr('-','_')] ||
        categories[name.downcase.tr(' ','_')]
      message << "\n  Did you mean '#{suggested_category}'?" if suggested_category

      fail_with message
    end
  end

  def assert_image
    return unless feature['image_url']

    img = File.join('source', feature['image_url'])
    fail_with "#{@file_path}: image #{img} not found" if !File.exist?(img)

    max_size = 150 * 1000
    size = File.size(img)
    return unless size > max_size

    fail_with "#{@file_path}: image #{img} is too large #{size}B"
  end
end

def releasePostItemSchema
  schema = YAML.load_file(File.join('bin','release-post-item-schema.yml'))

  rx = Rx.new({ :load_core => true })

  schema['custom_types'].each do |meta_schema|
    begin
      rx.learn_type(meta_schema['uri'], meta_schema['schema'])
    rescue Exception => e
      abort_with e.message
    end
  end

  begin
    schema = rx.make_schema(schema['schema'])
  rescue Exception => e
    abort_with e.message
  end

  return schema
end

def items
  search_path = ARGV.shift || 'data/release_posts/unreleased'

  unless File.file?(search_path) || File.directory?(search_path)
    abort_with "#{search_path} does not exist."
  end

  search_path = File.join(search_path, '*.{yaml,yml}') if File.directory?(search_path)

  return Dir.glob(search_path)
end

if $0 == __FILE__
  exit_code = 0

  schema = releasePostItemSchema()

  items().each do |i|
    begin
      ReleasePostEntry.new(i, schema).execute
    rescue ReleasePostHelpers::Exception => ex
      $stderr.puts ex.message
      exit_code = 1
    end
  end

  exit exit_code
end
